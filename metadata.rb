name "opsworks_cookbook_dw_tickets"
version "0.2.10"

maintainer       "Henk"
maintainer_email "meijerhenk@gmail.com"

recipe 'opsworks_cookbook_dw_tickets::setup', 'Run custom setup scripts.'
recipe 'opsworks_cookbook_dw_tickets::configure', 'Configure delayed_job worker.'
recipe 'opsworks_cookbook_dw_tickets::deploy', 'Deploy delayed_job worker.'
recipe 'opsworks_cookbook_dw_tickets::undeploy', 'Undeploy delayed_job worker.'
recipe 'opsworks_cookbook_dw_tickets::stop', 'Stop delayed_job worker.'

depends 'deploy'