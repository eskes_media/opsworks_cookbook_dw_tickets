# Adapted from unicorn::rails: https://github.com/aws/opsworks-cookbooks/blob/master/unicorn/recipes/rails.rb

include_recipe "opsworks_cookbook_dw_tickets::service"
# include_recipe "opsworks_cookbook_dw_tickets::ngx_pagespeed"

# setup delayed_job service per app
node[:deploy].each do |application, deploy|

  if deploy[:application_type] != 'rails'
    Chef::Log.debug("Skipping opsworks_cookbook_dw_tickets::setup application #{application} as it is not a Rails app")
    next
  end

  opsworks_deploy_user do
    deploy_data deploy
  end

  opsworks_deploy_dir do
    user deploy[:user]
    group deploy[:group]
    path deploy[:deploy_to]
  end

  
  # template "/etc/nginx/sites-enabled/dagjewegnl_tickets" do
  #   source 'nginx.conf'
  #   owner 'root'
  #   group 'root'
  #   mode '644'
  #   notifies :reload, node['openresty']['service']['resource']
  # end


  # script 'install phantomjs' do
  #   interpreter "bash"
  #   user "deploy"
  #   cwd "/tmp"
  #   code <<-EOH
  #     exists()
  #     {
  #       command -v "$1" >/dev/null 2>&1
  #       return 0
  #     }
  #     if exists phantomjs; then
  #       echo 'phantomjs exists!'
  #     else
  #       echo 'Installing phantomjs on this machine'
  #       sudo yum -y install fontconfig freetype freetype-devel fontconfig-devel libstdc++
  #       wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-1.9.8-linux-x86_64.tar.bz2
  #       sudo mkdir -p /opt/phantomjs
  #       bzip2 -d phantomjs-1.9.8-linux-x86_64.tar.bz2
  #       sudo tar -xvf phantomjs-1.9.8-linux-x86_64.tar --directory /opt/phantomjs/ --strip-components 1
  #       sudo ln -s /opt/phantomjs/bin/phantomjs /usr/bin/phantomjs
  #       return 0
  #     fi
  #     EOH
  # end

  # Allow deploy user to restart workers
  template "/etc/sudoers.d/#{deploy[:user]}" do
    mode 0440
    source "sudoer.erb"
    variables :user => deploy[:user]
  end

  template "#{node[:monit][:includes_dir]}/delayed_job_#{application}.monitrc" do
    mode 0644
    source "delayed_job.monitrc.erb"
    variables(:deploy => deploy, :application => application, :delayed_job => node[:delayed_job][application])

    notifies :reload, resources(:service => "monit"), :immediately
  end

end
